package jp.alhinc.yamaki_takeshi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

class CalculateSales {
	public static void main(String[] args)
	{
		//マップ生成　支店コード(キー)－支店名(値)の紐付け
		Map<String,String> branchNames = new HashMap<>();
	
		//マップ生成　支店コード(キー)－売上額(値)の紐付け
		Map<String,Long> branchSales = new HashMap<>();
		
		try 
		{
            File file = new File(args[0], "branch.lst");
            if (!file.exists()) {
				//エラー処理1.支店定義ファイル読み込み-1
				System.out.println("支店定義ファイルが存在しません");
				return;
            }
			BufferedReader br = null;
			try
			{
				//支店定義ファイル読み込み
				br = new BufferedReader(new FileReader(file));
				//売上額の初期化
				String line;
				while((line = br.readLine()) != null)
				{
					//文字列を分割するメソッド//
					String[] array = line.split(",", -1); 
					//エラー処理1-2
					if(array.length != 2 || !array[0].matches("[0-9]{3}"))
					{
						System.out.println("支店定義ファイルのフォーマットが不正です。");
                        return;
                    }
                    //支店コード－支店名の紐付け
                    branchNames.put(array[0], array[1]);
                    //支店コード(キー)－売上額(値)の紐付け
                    branchSales.put(array[0], 0L);
				}
				
			}
			finally
			{
				br.close();
			}

			//フィルタを作成
			FilenameFilter filter = new FilenameFilter()
			{
				public boolean accept(File dir, String filename)
				{
					//拡張子.rcdと数字8桁をチェック
					return new File(dir, filename).isFile() && filename.matches("^\\d{8}.rcd$");
				}
			};

			//コマンドライン指定されたディレクトリでファイルをフィルタリング
			File[] list = new File(args[0]).listFiles(filter);
			//ファイル名を入れる配列を作成
			int [] name = new int[list.length];
			for(int i = 0;i < list.length;i++)
			{
				//ファイル名取得
				String filename = list[i].getName();
				//ファイル名の数字8桁を取得
				String filename2 = filename.substring(0,8);
				//ファイル名の数字8桁を数値化して配列に入れる
				name[i] = Integer.parseInt(filename2);
			}
			//配列の中身（ファイル名の数字）を昇順に入れ替える
			Arrays.sort(name);
			//連番かどうかチェックする
			if(list.length - 1 != name[list.length - 1] - name[0])
			{
				//エラー処理2.集計-1
				System.out.println("売上ファイルが連番になっていません");
				return;
			}

			BufferedReader br2 = null;
			try
			{
				//売上ファイル読み込み
				for(int i = 0;i < list.length;i++)
				{
					br2 = new BufferedReader(new FileReader(list[i]));
					String line1;
					String line2;
					String line3;
					//支店コード読み込み
					line1 = br2.readLine();
					if(branchSales.containsKey(line1))
					{
						//エラー処理2.集計-3
						System.out.println(list[i].getName() + "の支店コードが不正です");
						return;
					}
					//売上額読み込み
					line2 = br2.readLine();
					//売上ファイルの3行目を読み込む
					line3 = br2.readLine();
					if(line3 != null)
					{
						//エラー処理2.集計-4
						System.out.println((list[i].getName()) + "のフォーマットが不正です");
                        return;
                    }
                    //売上額の数値化
                    Long value = Long.parseLong(line2);
                    //売上額加算
                    Long sum = branchSales.get(line1) + value;
                    if(sum.toString().length() > 10)
                    {
                        //エラー処理2.集計-2
                        System.out.println("合計金額が10桁を超えました");
                        return;
                    }
                    //System.out.println(sum);
                    //支店コード(キー)－売上額加算後(値)の紐付け
                    branchSales.put(line1,sum);
				}
				//System.out.println(branchSales.entrySet());
			}
			finally
			{
				br2.close();
			}
			//支店別集計ファイルを作成
			File newfile = new File(args[0],"branch.out");
			try
			{
				//支店別集計ファイルに書き込み
				File file = new File(args[0],"branch.out");
				BufferedWriter bw = new BufferedWriter(new FileWriter(file));
				for(Entry<String, String> entry : branchNames.entrySet())
				{
					bw.write(entry.getKey() + "," + entry.getValue() + "," + branchSales.get(entry.getKey()) + "\r\n");
				}
            }
            finally {
				bw.close();
            }
		}
		catch (Exception e)
		{
			//エラー処理3.その他	
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		
	}
}
